<?php
// This is the scoutandcast Page
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Scout & Cast - by Tyler Dow</title>
    <meta name="description" content="The website integrates information from a variety of resources and displays a large amount of data in one location, in a way that has previously never existed.">
    <?php include('../../header.php'); ?>

<div class="small-12 columns noPadding">
  <div id="scoutandcast" class="workProjectContainer">
    <div class="small-12 show-for-small-only columns noPadding">
      <header class="small-12 columns text-center">
        <img src="scoutandcast_logo.png" />
      </header>
    </div>
    <div class="small-12 medium-4 medium-push-8 large-3 large-push-9 columns noPadding projectDescription">
      <div class="small-12 columns clientOverviewLabel">PROJECT OVERVIEW</div>
      <div class="small-12 columns projectDescriptionOverview">
        <span class="overviewLabel">Client:</span>
        <h1>Scout &amp; Cast</h1>
        <span class="overviewLabel">Role:</span>
        <h2>Founder, Builder, One-Man Band</h2>
        <p>Scout &amp; Cast is a pet project of mine that has been in development from August 2013.</p>
        <p>The website integrates information from a variety of resources and displays a large amount of data in one location, in a way that has previously never existed.</p>
        <p>For this project, I designed, developed, marketed, managed and continually work on progressing all aspects of the website and business.</p>
        <p>There is a large amount of technology that is used in the site, from a slew of REST APIs, to the integration of CRM interactions.</p>
        <a title="View Scout and Cast" class="projectOverviewLink" href="http://scoutandcast.com/" target="_blank">View Scout &amp; Cast <i class="fa fa-angle-double-right"></i></a>
      </div>

      <div class="small-12 show-for-medium-up columns projectDescriptionBadges">
        <h3>Quick Highlights</h3>
        <div class="small-12 columns badgeContainer">
          <div class="badge"><i class="fa fa-user"></i></div>
          <div class="badgeLabel">Founder</div>
        </div>
        <div class="small-12 columns badgeContainer">
          <div class="badge"><i class="fa fa-cogs"></i></div>
          <div class="badgeLabel">Ran entire workflow</div>
        </div>
        <div class="small-12 columns badgeContainer end">
          <div class="badge"><i class="fa fa-dollar"></i></div>
          <div class="badgeLabel">Bootstrapped</div>
        </div>
        <div class="small-12 columns badgeContainer end">
          <div class="badge"><i class="fa fa-code"></i></div>
          <div class="badgeLabel">Integrated APIs</div>
        </div>
      </div>
    </div>

    <div class="small-12 medium-8 medium-pull-4 large-9 large-pull-3 columns noPadding projectExplanation">
      <header class="medium-12 show-for-medium-up columns text-center">
      	<img src="scoutandcast_logo.png" class="headerScreens" />
      </header>

      <div class="small-12 columns projectScreenshots">
        <img src="scoutandcast_devices.jpg" />
        <div class="small-12 columns screensArea">
          <div class="small-12 columns screensSection">
            <div class="screensSectionImage"><img src="screen_brand.jpg" class="screenshot" /></div>
            <div class="screensSectionText"><h2>Building an identity</h2><p>The brand needed to be fishing-oriented, yet not cliche or complex. Earth tones, subtle fishing elements and flat design rounded things out quite nicely.</p></div>
          </div>
          <div class="small-12 columns screensSection">
            <div class="screensSectionText"><h2>Organizing quality data into one place</h2><p>Using API data from USGS and multiple state agencies, I built Scout &amp; Cast to scratch the itch of some heavy front- and back-end development while creating something that fellow fishermen will love to use.</p></div>
            <div class="screensSectionImage"><img src="screen_1.jpg" class="screenshot" /></div>
          </div>
          <div class="small-12 columns text-center screensSectionText_full"><h2>Putting my entrepreneur skills to work</h2><p>While I built the entire site from the ground up, another passion of mine is running the business side of the "startup". This project required me to understand all facets of what it takes to run a startup and has allowed me to understand my website clients' needs even better.</p></div>
          <div class="small-12 columns screensSection">
            <div class="screensSectionImage"><img src="screen_2.jpg" class="screenshot" /></div>
            <div class="screensSectionText"><h2>Creating a directory of Fishing Pros</h2><p>I put on my salesman hat and began talking with fly shops and fishing guides in order to add them to a directory of fishing professionals. This coupling of fly shops and guides does not exist anywhere else on the web.</p></div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<?php include('../../footer.php'); ?>
