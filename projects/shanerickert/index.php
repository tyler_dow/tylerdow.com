<?php
// This is the Lazzara Lenton Page
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Shane Rickert Photography Project - Created by Tyler Dow</title>
    <meta name="description" content="After stumbling across Shane Rickert's photography on Flickr, I was able to get to know him and partnered with him while running Scout & Cast.">
    <?php include('../../header.php'); ?>

<div class="small-12 columns noPadding">
  <div id="ShaneRickert" class="workProjectContainer">
    <div class="small-12 show-for-small-only columns noPadding">
      <header class="small-12 columns text-center">
        <img src="shanerickert_logo.png" />
      </header>
    </div>
    <div class="small-12 medium-4 medium-push-8 large-3 large-push-9 columns noPadding projectDescription">
      <div class="small-12 columns clientOverviewLabel">PROJECT OVERVIEW</div>
      <div class="small-12 columns projectDescriptionOverview">
        <span class="overviewLabel">Client:</span>
        <h1>Shane Rickert Photography</h1>
        <span class="overviewLabel">Role:</span>
        <h2>Designer, Developer</h2>
        <p>After stumbling across Shane Rickert's photography on Flickr, I was able to get to know him and partnered with him while running Scout &amp; Cast.</p>
        <p>This was Shane Rickert's first website and thus, his first experience with the process involved with building a website.</p>
        <p>I came up with a website that uses a new CMS that seamlessly integrates into Adobe Lightroom and allows Shane to add photos to the site in a snap!</p>
        <a title="View ShaneRickert.com" class="projectOverviewLink" href="http://shanerickertphotography.com" target="_blank">View Shane Rickert Photography <i class="fa fa-angle-double-right"></i></a>
      </div>

      <div class="small-12 show-for-medium-up columns projectDescriptionBadges">
        <h3>Quick Highlights</h3>
        <div class="small-12 columns badgeContainer">
          <div class="badge"><i class="fa fa-arrow-up"></i></div>
          <div class="badgeLabel">Built on <a href="http://koken.me/" target="_blank">Koken CMS</a></div>
        </div>
        <div class="small-12 columns badgeContainer">
          <div class="badge"><i class="fa fa-wordpress"></i></div>
          <div class="badgeLabel">Taught web basics</div>
        </div>
        <div class="small-12 columns badgeContainer end">
          <div class="badge"><i class="fa fa-mobile"></i></div>
          <div class="badgeLabel">Integrates with Adobe</div>
        </div>
        <div class="small-12 columns badgeContainer end">
          <div class="badge"><i class="fa fa-mobile"></i></div>
          <div class="badgeLabel">Responsive</div>
        </div>
      </div>
    </div>

    <div class="small-12 medium-8 medium-pull-4 large-9 large-pull-3 columns noPadding projectExplanation">
      <header class="medium-12 show-for-medium-up columns text-center">
      	<img src="shanerickert_logo.png" />
      </header>

      <div class="small-12 columns projectScreenshots">
        <img src="shanerickert_devices.jpg" />
        <div class="small-12 columns screensArea">
          <div class="small-12 columns screensSection">
            <div class="screensSectionText"><h2>Built on Koken</h2><p><a href="http://koken.me/" target="_blank">Koken</a> is a new CMS that is catered toward photographers. When I took on this project I wanted to make sure it was easy to use and empowering for Shane. This was my first crack at using the Koken CMS and it was a huge success.</p></div>
            <div class="screensSectionImage"><img src="screen_1.jpg" class="screenshot" /></div>
          </div>
          <div class="small-12 columns screensSection">
            <div class="screensSectionImage"><img src="screen_2.png" class="screenshot" /></div>
            <div class="screensSectionText"><h2>Adobe Lightroom Integration</h2><p>With the power of Koken, Shane is able to simply press a couple of buttons to export new photos to the website from inside of Adobe Lightroom.</p></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include('../../footer.php'); ?>
