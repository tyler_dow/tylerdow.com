<?php
// This is the Lazzara Lenton Page
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Lazzara Lenton Yachts Project - Built by Tyler Dow</title>
    <meta name="description" content="After a rebrand of their company, Lazzara Lenton Yachts needed a website that would increase their exposure and allow them to become leaders among other yacht brokers online.">
    <?php include('../../header.php'); ?>

<div class="small-12 columns noPadding">
  <div id="LazzaraLenton" class="workProjectContainer">
    <div class="small-12 show-for-small-only columns noPadding">
      <header class="small-12 columns text-center">
        <img src="lazzaralenton_logo.png" />
      </header>
    </div>
    <div class="small-12 medium-4 medium-push-8 large-3 large-push-9 columns noPadding projectDescription">
      <div class="small-12 columns clientOverviewLabel">PROJECT OVERVIEW</div>
      <div class="small-12 columns projectDescriptionOverview">
        <span class="overviewLabel">Client:</span>
        <h1>Lazzara Lenton</h1>
        <span class="overviewLabel">Role:</span>
        <h2>UX, Designer, Developer, SEO</h2>
        <p>After a rebrand of their company, Lazzara Lenton Yachts needed a website that would increase their exposure and allow them to become leaders among other yacht brokers online.</p>
        <p>The website integrates with YachtWorld.com's variety of services and solutions to deliver a modern, elegant medium to finding a new yacht with Lazzara Lenton.</p>
        <p>Everything about the website has been designed to reflect the experience you get when you work with Lazzara Lenton - a respected and iconic member of the yacht industry.</p>
        <a title="View Lazzara Lenton Yachts" class="projectOverviewLink" href="http://lazzaralenton.com" target="_blank">View Lazzara Lenton Yachts <i class="fa fa-angle-double-right"></i></a>
      </div>

      <div class="small-12 show-for-medium-up columns projectDescriptionBadges">
        <h3>Quick Highlights</h3>
        <div class="small-12 columns badgeContainer">
          <div class="badge"><i class="fa fa-arrow-up"></i></div>
          <div class="badgeLabel">Improved SEO</div>
        </div>
        <div class="small-12 columns badgeContainer">
          <div class="badge"><i class="fa fa-wordpress"></i></div>
          <div class="badgeLabel">Built on Wordpress</div>
        </div>
        <div class="small-12 columns badgeContainer end">
          <div class="badge"><i class="fa fa-mobile"></i></div>
          <div class="badgeLabel">Responsive</div>
        </div>
      </div>
    </div>

    <div class="small-12 medium-8 medium-pull-4 large-9 large-pull-3 columns noPadding projectExplanation">
      <header class="medium-12 show-for-medium-up columns text-center">
      	<img src="lazzaralenton_logo.png" />
      </header>

      <div class="small-12 columns projectScreenshots">
        <img src="lazzaralenton_devices.jpg" />
        <div class="small-12 columns screensArea">
          <div class="small-12 columns screensSection">
            <div class="screensSectionText"><h2>A clean, responsive showcase for luxury yachts</h2><p>In a sea (pun intended) of mediocre yacht sales websites, Lazzara Lenton realized the importance of a great web precense and we focused the effort into making a website that is clean, easy to use and reflects a "luxury yacht experience".</p></div>
            <div class="screensSectionImage"><img src="screen_2.jpg" class="screenshot" /></div>
          </div>
          <div class="small-12 columns text-center screensSectionText_full"><h2>The best implementation of YachtWorld API data</h2><p>Yachtworld.com recently opened up their API to enable access to a directory of over 100,000 yachts on the market. Using this data and focusing on mobile and SEO, I created a solution that is like none-other on the web.</p></div>
          <img src="screen_3.jpg" class="screenshot" />
          <img src="screen_1.jpg" class="screenshot" />
        </div>
      </div>
    </div>
  </div>
</div>

<?php include('../../footer.php'); ?>
