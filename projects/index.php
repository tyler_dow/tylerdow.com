<?php
// This is the Projects Page
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Projects | Tyler Dow - Web Designer</title>
    <?php include('../header.php'); ?>

<div class="small-12 columns projectsPage noPadding">
  <header>
    <h1>Projects</h1>
  </header>

  <div id="itemWayfinders" class="small-12 columns projectsPageItem">
    <div class="small-6 large-4 large-offset-2 columns projectImageContainer">
      <a href="wayfinders/" class="projectLogo"><img src="wayfinders/wayfinders_logo.png" /></a>
      <a href="wayfinders/" class="browserShot"><img src="img/wayfinders_screen.jpg" /></a></div>
    <div class="small-6 large-4 end columns projectTitleContainer">
      <div class="projectTitleText">
        <h4>Seattle Wayfinders</h4><h5>Role: Developer</h5><a class="small button success radius projectLauncherButton" href="wayfinders/">View Project</a>
      </div>
    </div>
  </div>

  <div id="itemLazzaraLenton" class="small-12 columns projectsPageItem">
    <div class="small-6 large-4 large-offset-2 columns projectImageContainer">
      <a href="lazzaralenton/" class="projectLogo"><img src="lazzaralenton/lazzaralenton_logo.png" /></a>
      <a href="lazzaralenton/" class="browserShot"><img src="img/lazzaralenton_screen.jpg" /></a>
    </div>
    <div class="small-6 large-4 end columns projectTitleContainer">
      <div class="projectTitleText">
        <h4>Lazzara Lenton</h4><h5>Role: UX, Designer, Developer, SEO</h5><a class="small button success radius projectLauncherButton" href="lazzaralenton/">View Project</a>
      </div>
    </div>
  </div>

  <div class="small-12 columns projectsPageGrid">
    <div id="itemScoutandCast" class="small-12 columns projectsPageItem">
      <div class="small-6 large-4 large-offset-2 columns projectImageContainer">
        <a href="scoutandcast/" class="projectLogo"><img src="scoutandcast/scoutandcast_logo.png" /></a>
        <a href="scoutandcast/" class="browserShot"><img src="img/scoutandcast_screen.jpg" /></a>
      </div>
      <div class="small-6 large-4 end columns projectTitleContainer">
        <div class="projectTitleText">
          <h4>Scout &amp; Cast</h4><h5>Role: Founder, Builder</h5><a class="small button success radius projectLauncherButton" href="scoutandcast/">View Project</a>
        </div>
      </div>
    </div>

    <div id="itemIVLab" class="small-12 columns projectsPageItem">
      <div class="small-6 large-4 large-offset-2 columns projectImageContainer">
        <a href="iv_lab/" class="projectLogo"><img src="iv_lab/IVLab_logo.png" /></a>
        <a href="iv_lab/" class="browserShot"><img src="img/iv_lab_screen.jpg" /></a>
      </div>
      <div class="small-6 large-4 end columns projectTitleContainer">
        <div class="projectTitleText">
          <h4>IV Laboratory</h4><h5>Role: Developer, Designer</h5><a class="small button success radius projectLauncherButton" href="iv_lab/">View Project</a>
        </div>
      </div>
    </div>

    <div id="itemShaneRickert" class="small-12 columns projectsPageItem">
      <div class="small-6 large-4 large-offset-2 columns projectImageContainer">
        <a href="shanerickert/" class="projectLogo"><img src="shanerickert/shanerickert_logo.png" /></a>
        <a href="shanerickert/" class="browserShot"><img src="img/shanerickert_screen.jpg" /></a>
      </div>
      <div class="small-6 large-4 end columns projectTitleContainer">
        <div class="projectTitleText">
          <h4>Shane Rickert</h4><h5>Role: Developer, Designer</h5><a class="small button success radius projectLauncherButton" href="shanerickert/">View Project</a>
        </div>
      </div>
    </div>


  </div>
</div>



<?php include('../footer.php'); ?>
