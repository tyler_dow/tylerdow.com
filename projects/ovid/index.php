<?php
// This is the Lazzara Lenton Page
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Ovid Vineyards Project - Built by Tyler Dow</title>
    <meta name="description" content="I developed a custom, responsive Wordpress theme that allows Ovid to have control over their website in ways that they did not previously have. The theme also integrates with their current database of members and subscribers to allow users to easily log into their accounts from the Ovid website.">
    <?php include('../../header.php'); ?>

<div class="small-12 columns noPadding">
  <div id="ovid" class="workProjectContainer">
    <div class="small-12 show-for-small-only columns noPadding">
      <header class="small-12 columns text-center">
        <img src="ovid_logo.png" />
      </header>
    </div>
    <div class="small-12 medium-4 medium-push-8 large-3 large-push-9 columns noPadding projectDescription">
      <div class="small-12 columns clientOverviewLabel">PROJECT OVERVIEW</div>
      <div class="small-12 columns projectDescriptionOverview">
        <span class="overviewLabel">Client:</span>
        <h1>Ovid Vineyards</h1>
        <span class="overviewLabel">Role:</span>
        <h2>Developer</h2>
        <p>Ovid Vineyards is an elegant and classic vineyard in California's Napa Valley.</p>
        <p>I developed a custom, responsive Wordpress theme that allows Ovid to have control over their website in ways that they did not previously have. The theme also integrates with their current database of members and subscribers to allow users to easily log into their accounts from the Ovid website.</p>
        <a class="projectOverviewLink" href="http://ovidvineyards.com" target="_blank">View Ovid Vineyards <i class="fa fa-angle-double-right"></i></a>
      </div>

      <div class="small-12 show-for-medium-up columns projectDescriptionBadges">
        <h3>Quick Highlights</h3>
        <div class="small-12 columns badgeContainer">
          <div class="badge"><i class="fa fa-wordpress"></i></div>
          <div class="badgeLabel">Built on Wordpress</div>
        </div>
        <div class="small-12 columns badgeContainer end">
          <div class="badge"><i class="fa fa-mobile"></i></div>
          <div class="badgeLabel">Responsive</div>
        </div>
      </div>
    </div>

    <div class="small-12 medium-8 medium-pull-4 large-9 large-pull-3 columns noPadding projectExplanation">
      <header class="medium-12 show-for-medium-up columns text-center">
      	<img src="ovid_logo.png" />
      </header>

      <div class="small-12 columns projectScreenshots">
        <img src="ovid_devices.jpg" class="headerScreens" />
        <div class="small-12 columns screensArea">
          <div class="small-12 columns screensSection">
            <div class="screensSectionText"><h2>A unique and modern brand</h2><p>This isn't the typical caligraphy-filled brand that has become synomomous with Nappa-Valley wineries. Their website needed to escape that cliche as well.</p></div>
            <div class="screensSectionImage"><img src="screen_1.jpg" class="screenshot" /></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include('../../footer.php'); ?>
