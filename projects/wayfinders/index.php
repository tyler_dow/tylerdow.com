<?php
// This is the Wayfinders Page
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Seattle Wayfinders Project - Developed by Tyler Dow</title>
    <meta name="description" content="Seattle Wayfinders is an effort led by AIGA Seattle and Design In Public</a>, to promote and encourage commuting to work in the Seattle area.">
    <?php include('../../header.php'); ?>

<div class="small-12 columns noPadding">
  <div id="wayfinders" class="workProjectContainer">
    <div class="small-12 show-for-small-only columns noPadding">
      <header class="small-12 columns text-center">
        <img src="wayfinders_logo.png" />
      </header>
    </div>
    <div class="small-12 medium-4 medium-push-8 large-3 large-push-9 columns noPadding projectDescription">
      <div class="small-12 columns clientOverviewLabel">PROJECT OVERVIEW</div>
      <div class="small-12 columns projectDescriptionOverview">
        <span class="overviewLabel">Client:</span>
        <h1>Seattle Wayfinders</h1>
        <span class="overviewLabel">Role:</span>
        <h2>Developer</h2>
        <p>Seattle Wayfinders is an effort led by <a href="http://seattle.aiga.org/">AIGA Seattle</a> and <a href="http://designinpublic.org/">Design In Public</a>, to promote and encourage commuting to work in the Seattle area.</p>
        <p>I was asked by members of the AIGA Seattle board of directors to develop this website on a quick deadline, for a mostly mobile audience.</p>
        <p>The site was featured in conjunction with a mural, painted in a prominent area of downtown Seattle.</p>
        <a title="View Seattle Wayfinders" class="projectOverviewLink" href="http://seattlewayfinders.org/" target="_blank">View Seattle Wayfinders <i class="fa fa-angle-double-right"></i></a>
      </div>

      <div class="small-12 show-for-medium-up columns projectDescriptionBadges">
        <h3>Quick Highlights</h3>
        <div class="small-12 columns badgeContainer">
          <div class="badge"><i class="fa fa-bolt"></i></div>
          <div class="badgeLabel">Fast Turnaround</div>
        </div>
        <div class="small-12 columns badgeContainer">
          <div class="badge"><i class="fa fa-check"></i></div>
          <div class="badgeLabel">No CMS Needed</div>
        </div>
        <div class="small-12 columns badgeContainer end">
          <div class="badge"><i class="fa fa-mobile"></i></div>
          <div class="badgeLabel">Mobile-First</div>
        </div>
      </div>
    </div>

    <div class="small-12 medium-8 medium-pull-4 large-9 large-pull-3 columns noPadding projectExplanation">
      <header class="medium-12 show-for-medium-up columns text-center">
      	<img src="wayfinders_logo.png" />
      </header>

      <div class="small-12 columns projectScreenshots">
        <img src="wayfinders_devices.jpg" />
        <div class="small-12 columns screensArea">
          <div class="small-12 columns text-center screensSectionText_full"><h2>From the client:</h2><p>Working with Tyler on AIGA’s Wayfinders project was instrumental to the success of our event. We went to Tyler because he has a solid understanding of design that complements his strong development skills. He was easy to work with, very responsive and delivered exactly what we needed. On top of having him develop the site, we approached him (after the fact) about developing our mobile responsive site which effectively doubled his work load with the same time restraints. He was extremely accommodating and helpful and made our experience a great one. The event attracted several thousand people and  hundreds of participants including coverage from the Seattle Times. Thanks for everything, Tyler!<br><strong>David Miller – Community Outreach Director | AIGA Seattle</strong></p></div>
        </div>
      </div>
    </div>


  </div>
</div>

<?php include('../../footer.php'); ?>
