<?php
// This is the Wayfinders Page
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>IV Lab Project - Developed by Tyler Dow</title>
    <?php include('../../header.php'); ?>

<div class="small-12 columns noPadding">
  <div id="IVLab" class="workProjectContainer">
    <div class="small-12 show-for-small-only columns noPadding">
      <header class="small-12 columns text-center">
        <img src="IVLab_logo.png" />
      </header>
    </div>
    <div class="small-12 medium-4 medium-push-8 large-3 large-push-9 columns noPadding projectDescription">
      <div class="small-12 columns clientOverviewLabel">PROJECT OVERVIEW</div>
      <div class="small-12 columns projectDescriptionOverview">
        <span class="overviewLabel">Client:</span>
        <h1>IV Lab</h1>
        <span class="overviewLabel">Role:</span>
        <h2>Developer, Designer</h2>
        <p>The Intellectual Ventures Laboratory is a mad scientist's dream lab.</p>
        <p>The staff at the lab were transitioning from an existing Wordpress site to the new website, built on ExpressionEngine - which was a large, complex undertaking.</p>
        <p>I served as a member of the two-person development team that built out the new site and managed the transition of content and branding.</p>
        <p>Using non-conventional development tactics, we were able to create page layouts that made it easy to add content in a modular, "plug and play" way.</p>
        <p>The use of video and javaScript interactions are featured throughout the site and respond to browser size with speed with ease.</p>
        <a title="View IV Laboratory" class="projectOverviewLink" href="http://www.intellectualventureslab.com/" target="_blank">View IV Lab <i class="fa fa-angle-double-right"></i></a>
      </div>

      <div class="small-12 show-for-medium-up columns projectDescriptionBadges">
        <h3>Quick Highlights</h3>
        <div class="small-12 columns badgeContainer">
          <div class="badge"><i class="fa fa-users"></i></div>
          <div class="badgeLabel">Team Build</div>
        </div>
        <div class="small-12 columns badgeContainer">
          <div class="badge"><i class="fa fa-video-camera"></i></div>
          <div class="badgeLabel">HTML5 Video</div>
        </div>
        <div class="small-12 columns badgeContainer">
          <div class="badge"><i class="fa fa-mobile"></i></div>
          <div class="badgeLabel">Responsive</div>
        </div>
        <div class="small-12 columns badgeContainer">
          <div class="badge"><i class="fa fa-code"></i></div>
          <div class="badgeLabel">ExpressionEngine CMS</div>
        </div>
      </div>
    </div>

    <div class="small-12 medium-8 medium-pull-4 large-9 large-pull-3 columns noPadding projectExplanation">
      <header class="medium-12 show-for-medium-up columns text-center">
      	<img src="IVLab_logo.png" />
      </header>

      <div class="small-12 columns projectScreenshots">
        <img src="ivlab_devices.jpg" />
      </div>
    </div>
  </div>
</div>

<?php include('../../footer.php'); ?>
