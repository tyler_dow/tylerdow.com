<?php
// This is the footer
?>

      <footer>
        <div class="small-12 medium-4 columns footerSocialLinks">
          <a class="small-2 columns text-center" title="Tyler Dow on Twitter" href="https://twitter.com/tyler_dow"><i class="fa fa-twitter"></i></a>
          <a class="small-2 columns text-center" title="Tyler Dow on Instagram" href="http://instagram.com/tyler_dow"><i class="fa fa-instagram"></i></a>
          <a class="small-2 columns text-center" title="Tyler Dow on LinkedIn" href="https://www.linkedin.com/in/dow0013"><i class="fa fa-linkedin-square"></i></a>
          <a class="small-2 columns text-center" title="Tyler Dow on Google+" href="https://plus.google.com/+TylerDow13/posts"><i class="fa fa-google-plus"></i></a>
          <a class="small-2 columns text-center" title="Tyler Dow on Dribbble" href="https://dribbble.com/tyler_dow"><i class="fa fa-dribbble"></i></a>
          <a class="small-2 columns text-center end" title="Tyler Dow on Angel List" href="https://angel.co/tyler-dow"><i class="fa fa-angellist"></i></a>
        </div>
        <div class="small-12 medium-8 columns small-text-center medium-text-right">
          <h5>&copy; <?php echo date("Y") ?> Tyler Dow | <a title="Let's make something awesome" class="contactMe">Let's make something awesome.</a></h5>
        </div>
      </footer>

    </div><!-- ends pageWrapperMain -->

    <div id="contactForm">
      <form action='//tylerdow.activehosted.com/proc.php' method='post' id='_form_1012' accept-charset='utf-8' enctype='multipart/form-data'>
        <div id="contactClose">x</div>
        <input type='hidden' name='f' value='1012'>
        <input type='hidden' name='s' value=''>
        <input type='hidden' name='c' value='0'>
        <input type='hidden' name='m' value='0'>
        <input type='hidden' name='act' value='sub'>
        <input type='hidden' name='nlbox[]' value='1'>
        <div class='_form'>
          <div class='formwrapper small-12 columns'>
            <div class="small-12 medium-6 columns cardLeft">
              <img src="/img/postcardHeader.png" class="postcardGreeting" />
              <p class="postcardWitt">I'm having a GREAT time at TylerDow.com!</p>
              <div id='_field39' class="postcardName">
                <div id='compile39' class='_field _type_input'>
                  <div class='_option'>
                    <input type='text' name='fullname' placeholder="Your Name" >
                  </div>
                </div>
              </div>
              <div id='_field40' class="postcardEmail">
                <div id='compile40' class='_field _type_input'>
                  <div class='_option'>
                    <input type='email' name='email' placeholder="*Email" >
                  </div>
                </div>
              </div>
            </div>
            <div class="small-12 medium-6 columns">
              <div id='_field43' class="postcardMessage">
                <div class='_label postcardMessageLabel'>
                  Your Message:
                </div>
                <div id='compile43' class='_field _type_textarea'>
                  <div class='_option'>
                    <textarea rows="5" name='field[2]'></textarea>
                  </div>
                </div>
              </div>
              <div id='_field41' class="postcardSend">
                <div id='compile41' class='_field _type_input'>
                  <div class='_option'>
                    <input class="postcardSendButton button small radius" type='submit' value="Send!">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div><!-- ends contactForm -->

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-37001375-1', 'auto');
      ga('send', 'pageview');

    </script>

    <script src="/js/vendor/modernizr.js"></script>
    <script src="/js/vendor/jquery.js"></script>
    <script src="/js/foundation.min.js"></script>
    <script src="/js/vendor/unslider.js"></script>
    <script>

      $(document).foundation();

      var windowHeight = $(window).height();
      var windowWidth = $(window).width();
      var videoSize = windowHeight-50;

      var aspectRatio = windowWidth/videoSize;

      $(window).scroll(function() {
          var scroll = $(window).scrollTop();
          var scrollVariable = windowHeight-20;

          if (scroll >= windowHeight-scrollVariable) {
              $(".topHeader").addClass("darkHeader");
          }
          if (scroll <= windowHeight-scrollVariable) {
              $(".topHeader").removeClass("darkHeader");
          }
      });

      $('#drawerLauncher .fa-bars').click(function(){
        $("nav").toggleClass('drawerOpen');
        $("#pageWrapperMain").toggleClass('drawerOpen');
      })

      $('.contactMe').click(function(){
        $("#contactForm").toggleClass('sendingNow');
      })
      $('#contactClose').click(function(){
        $("#contactForm").toggleClass('sendingNow');
      })

      $('.projectsPageItem').hover(function(){
             $(this).addClass('active');
           },
           function() {
             $(this).removeClass('active');
           }
       );//

       var unslider = $('.banner').unslider({
      	speed: 600,               //  The speed to animate each slide (in milliseconds)
      	delay: false,              //  The delay between slide animations (in milliseconds)
      	complete: function() {},  //  A function that gets called after every slide animation
      	keys: true,               //  Enable keyboard (left, right) arrow shortcuts
      	dots: false,               //  Display dot navigation
      	fluid: false              //  Support responsive design. May break non-responsive designs
      });

        $('.unslider-arrow').click(function() {
            var fn = this.className.split(' ')[1];
            unslider.data('unslider')[fn]();
        });


      document.getElementById('homeContainer').setAttribute("style","height:"+videoSize+"px");
      $(document).ready(function(){
        if(aspectRatio > 1.7777777){
          document.getElementById('video1').setAttribute("style","width:100%;height:auto;");
        } else {
          document.getElementById('video1').setAttribute("style","width:auto;height:100%;");
        }

      });

    setTimeout(function(){var a=document.createElement("script");
    var b=document.getElementsByTagName("script")[0];
    a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0023/4489.js?"+Math.floor(new Date().getTime()/3600000);
    a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
    </script>
  </body>
</html>
