<?php
// This is the header file
?>
    <link rel="icon" type="image/png" href="http://tylerdow.com/favicon.png" />
    <link rel="stylesheet" href="/css/foundation.min.css" />
    <link rel="stylesheet" href="/css/style.min.css" />
    <link rel="stylesheet" href="/css/font-awesome.min.css" />

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'>

  </head>
  <body>
    <nav class="links" itemscope itemtype="http://schema.org/Person">
      <a id="drawerLogo" href="/"><img itemprop="image" src="/media/tyler_dow.jpg" /></a>
      <div class="TylerDow">
        <h1 itemprop="name">Tyler Dow</h1>
        <span itemprop="jobTitle">Web Designer/Developer</span><br>
        <a itemprop="email" href="mailto:tyler.dow13@gmail.com" title="e-mail">tyler.dow13@gmail.com</a>
      </div>
      <div>
        <ul>
          <li><a title="Tyler Dow" itemprop="url" href="http://tylerdow.com">Home</a></li>
          <li><a title="About Tyler Dow" href="/about/">About</a></li>
          <li><a title="Tyler Dow's Projects" href="/projects/">Projects</a></li>
        </ul>
      </div>

      <p>I believe Pixels &amp; Code can be used to empower and facilitate great contributions to the world.<br><br><i class="fa fa-quote-left"></i> <i class="fa fa-quote-right"></i></p>
    </nav>

    <div id="pageWrapperMain">
      <div class="small-12 columns topHeader">
        <div id="drawerLauncher" class="small-2 columns navLauncher">
          <i class="fa fa-bars"></i>
          <a title="Contact Tyler Dow" class="contactMe headerContact"><i class="fa fa-paper-plane-o"></i> contact</a>
        </div>
      </div>
