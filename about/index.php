<?php
// This is the Projects Page
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Tyler Dow - Web Designer</title>
    <?php include('../header.php'); ?>

<div class="small-12 columns projectsPage noPadding">
  <header id="aboutHeader">
    <h1>Tyler Dow</h1>
    <h3>a designer, developer, builder, entrepreneur, adventurer, etc.</h3>
  </header>
  <div class="small-12 columns aboutPage">
    <?php if($_GET["sentmail"] == "true"){ ?>
      <div class="postSuccess">Your message was safely delivered to my inbox. Thanks for reaching out!</div>
    <?php } ?>
    <div class="small-12 medium-8 columns aboutPageStory">
      <h4>Who am I?</h4>
      <p style="margin-bottom:10px;">I'm a midwestern raised, west coast loving wanderer. I've been known to always have the "next big idea" stirring around in my head.</p><p>My prerogative in life is to empower other people, clients and friends to be successful in whatever it is that makes them happy.</p>

      <h4>What do I do?</h4>
      <p>Well... I'm a web designer, but I'm also a web developer. My coding skills can reside in both front-end and back-end environments. I run my own startup on the side and I also know how to manage web-projects quite well. You could even consider me a photographer, videographer or editor. I've produced small broadcast TV shows and have even won an award for being a successful entrepreneur. There are few creative avenues I won't venture down, but I'm most successful when I'm building great projects with pixels and code.</p>

      <h4>Where am I located?</h4>
      <p>That's a great question. My wife and I are currently in amidst our own little adventure that is taking us all over the country. She's an incredible travel nurse, which allows us be in different parts of the country every three months. We are currently in Seattle, WA.<!--If you care to keep up with our wanderings, you can learn more at <a href="http://followthedows.com">FollowTheDows.com</a>.--></p>
    </div>
    <div class="small-12 medium-4 columns aboutPageImages">
      <h5>Wanna make something cool? Send me a message and let's get started.</h5>
      <form action='//tylerdow.activehosted.com/proc.php' method='post' id='_form_1012' accept-charset='utf-8' enctype='multipart/form-data'>
        <input type='hidden' name='f' value='1012'>
        <input type='hidden' name='s' value=''>
        <input type='hidden' name='c' value='0'>
        <input type='hidden' name='m' value='0'>
        <input type='hidden' name='act' value='sub'>
        <input type='hidden' name='nlbox[]' value='1'>
        <div class='_form'>
          <div class='formwrapper'>
            <div id='_field39'>
              <div id='compile39' class='_field _type_input'>
                <div class='_label '>
                  Full Name
                </div>
                <div class='_option'>
                  <input type='text' name='fullname' >
                </div>
              </div>
            </div>
            <div id='_field40'>
              <div id='compile40' class='_field _type_input'>
                <div class='_label '>
                  Email *
                </div>
                <div class='_option'>
                  <input type='email' name='email' >
                </div>
              </div>
            </div>
            <div id='_field43'>
              <div id='compile43' class='_field _type_textarea'>
                <div class='_label '>
                  Your Message
                </div>
                <div class='_option'>
                  <textarea rows="5" name='field[2]'></textarea>
                </div>
              </div>
            </div>
            <div id='_field41'>
              <div id='compile41' class='_field _type_input'>
                <div class='_option'>
                  <input class="button small radius" type='submit' value="Contact">
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<?php include('../footer.php'); ?>
