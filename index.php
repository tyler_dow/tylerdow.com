<?php
// This is the homepage
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Tyler Dow | Web Designer/Developer Portfolio</title>
    <?php include('header.php'); ?>
  <div class="row HomeVideo" id="homeContainer">
    <video id="video1" autoplay loop muted>
     <source src="media/tylerdow.mp4" type='video/mp4' />
     <source src="media/tylerdow.webm" type='video/webm' />
     <source src="media/tylerdow.ogv" type='video/ogg' />
    </video>
    <div class="videoOverlay">
      <div class="openingMessage">
        <h1><span class="creative">Plan. Design. Build.</span><br>let's create awesome websites</h1>
      </div>
    </div>
  </div>

  <div id="locationPanel" class="row">
    <div class="small-12 columns" style="padding:0;">
    	<div class="panel small-12 columns">
        <div class="small-12 medium-8 medium-centered columns text-center travelMessage">
          <h3>Currently wayfaring.</h3>
          <p>Armed with a computer and an internet connection, I'm spending my days in and out of coffee shops and co-working spaces - building stellar websites for some incredible clients.</p>
        </div>
        <div class="small-12 columns text-center end currentCity city">
          <h5>Current City</h5>
          <a><i class="fa fa-map-marker"></i>&nbsp;&nbsp;Seattle, WA</a>
        </div>
    	</div>
    </div>
  </div>

  <div id="skillsPanel" class="row noPadding">
    <div class="small-12 columns noPadding">
      <div class="small-12 large-4 columns text-center skillSet skillUX" style="background-color:#6C8A82">
        <i class="fa fa-child"></i><br>
        <h3>UX</h3>
        <p>The old saying, "Measure twice, Cut once" also works when building a website. However, it's rare to only measure twice... UX Design is where I walk clients through the planning and layout of the website in order to nail down exactly what we need to create.</p>
      </div>
      <div class="small-12 large-4 columns text-center skillSet skillDesign" style="background-color:#8D8678">
        <i class="fa fa-pencil"></i><br>
        <h3>Design</h3>
        <p>Regardless of what we're creating, it needs to look good. During the design of the project, I'll put together static website designs as well as dynamic, in-browser versions to make sure that the project maintains it's look across all screen shapes and sizes.</p>
      </div>
      <div class="small-12 large-4 columns text-center skillSet skillDevelopment" style="background-color:#C95748">
        <i class="fa fa-code"></i><br>
        <h3>Development</h3>
        <p>"If you build it, they will come"... well... as long as it's optimized for search engines, easy to share, engaging and loads quickly. No matter the size or intensity of the project, I enjoy making sure things are built well &amp; are easy to maintain.</p>
      </div>
    </div>
  </div>

  <div id="quotePanel" class="row columns text-center">
    <div class="banner">
        <ul>
            <li>" Tyler is one of the most creative, and enthusiastic people I have ever met. His design skills, thirst for knowledge, and willingness to learn are truly inspiring. I can personally vouch that his presence immeasurably improved our team's output. As Tyler grows, he inspires those around him to grow with him. "<br><strong>- Nathon Sims</strong></li>
            <li>" Working with Tyler on AIGA’s Wayfinders project was instrumental to the success of our event. We went to Tyler because he has a solid understanding of design that complements his strong development skills. He was easy to work with, very responsive and delivered exactly what we needed. "<br><strong>- David Miller</strong></li>
        </ul>
    </div>
    <a class="unslider-arrow next">another quote <i class="fa fa-caret-square-o-right"></i></a>

  </div>

  <div id="explorePanel" class="row adventure">
    <div class="small-12 medium-5 large-offset-1 end columns">
      <h2>Explore.</h2>
      <p>My life is fueled by adventure and wonder. This never ending curiosity spills over into my work and the projects I work on. No one want's a cookie-cutter website and I'm not going to create one. It's my prerogative to get my clients excited and empowered by their websites while always strategizing about how to make them even better.<br><br><a class="contactMe" title="Let's Get Started">Let's get started <i class="fa fa-angle-double-right"></i></a></p>
    </div>
  </div>

  <div id="articlesPanel" class="row">
    <h3>Recent Ramblings</h3>
    <div class="small-12 medium-6 columns"><p><strong><a title="Tyler Dow's Post on Medium | Be an Enabler" href="https://medium.com/@tyler_dow/be-an-enabler-a250d5005bdb" target"_blank">Be an “Enabler”</a></strong><br>The core of my job as a web designer/developer is to create things on the web for people who can’t create those things themselves. I’ve recently begun to think of myself as an “Enabler”, which has radically changed my...</p></div>
    <div class="small-12 medium-6 columns"><p><strong><a title="Tyler Dow's Post on Medium | Everyone is a designer" href="https://medium.com/@tyler_dow/everyone-is-a-designer-a386bedb67cd" target"_blank">Everyone is a designer</a></strong><br>As I meet more and more people in the tech industry, I am increasingly faced with “I’m a designer” when I ask someone what they do. The first few times I heard this, I chalked the person up to being a...</p></div>
    <a class="mediumBlogLink" href="https://medium.com/@tyler_dow" title="Medium.com" target"_blank">read more on Medium.com</a>
  </div>



<?php include('footer.php'); ?>
